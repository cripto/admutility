﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Usb_device_coding
{
    class Combobox_DataSource
    {
        public Combobox_DataSource(string DisplayMember, string ValueMember)
        {
            this.DisplayMember = DisplayMember;
            this.ValueMember = ValueMember;
        }

        public string DisplayMember {get; private set;}
        public string ValueMember { get; private set; }
    }
}
