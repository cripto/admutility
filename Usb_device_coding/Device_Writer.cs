﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Threading;


using Ata;

using System.Management;
using Helper.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Collections;


// This class recording and reading data from device sectors, and must to do erase of selecteddevice.
namespace Usb_device_coding
{
    class Device_Writer
    {
        const uint OPEN_EXISTING = 3;
        const uint GENERIC_READ = 0x80000000;
        const uint GENERIC_WRITE = 0x40000000;

        //static SafeFileHandle DeviceHandle;

        [DllImport("kernel32", SetLastError = true)]
        static extern SafeFileHandle CreateFile
        (
            string FileName,            //file name
            uint DesiredAccess,         //acess mode
            uint ShareMode,             //share mode
            uint SecurityAttributes,    //security attributes
            uint CreationDisposition,   //how to create
            uint FlagsAndAttributes,    //file attributes
            int hTemplateFile           //handle to template file
         );

        public static byte[] Read(string Device, long ReadPosition, int SizeToRead) // reading SizeToRead bytes from Position. Parameter 'Device' must be something like @"\\.\G:"
        {
            byte[] buffer = new byte[SizeToRead];


           SafeFileHandle DeviceHandle = CreateFile(Device, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
            if (DeviceHandle.IsInvalid)
            {
                throw new IOException("Unable to access drive. Win32 Error Code " + Marshal.GetLastWin32Error());
                //if get windows error code 5 this means access denied. You must try to run the program as admin privileges.
            }
            else
            {
                FileStream ReadStream = new FileStream(DeviceHandle, FileAccess.ReadWrite);

                ReadStream.Position = ReadPosition;//0x200 or 512 in bytes, because first sector can be systemic;


                
                try
                {
                    ReadStream.Read(buffer, 0, SizeToRead);
                }
                catch
                {
                    throw new IOException("Reading error " + Marshal.GetLastWin32Error());
                }
                try
                {
                    ReadStream.Dispose();
                    DeviceHandle.Dispose(); // FileStream realise IDisposable interface.
                }
                catch
                {
                    throw new IOException("Unable to close drive. Win32 Error Code " + Marshal.GetLastWin32Error());
                }

                return buffer;
            }
        }


        public static void Write(string Device, long WritePosition, byte[] BytesToWrite) // writing array of bytes from Position. Parameter 'Device' must be something like @"\\.\G:"
        {

            SafeFileHandle DeviceHandle = CreateFile(Device, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
            if (DeviceHandle.IsInvalid)
            {
                throw new IOException("Unable to access drive. Win32 Error Code " + Marshal.GetLastWin32Error());
                //if get windows error code 5 this means access denied. You must try to run the program as admin privileges.
            }
            else
            {
                FileStream WriteStream = new FileStream(DeviceHandle, FileAccess.ReadWrite);
                WriteStream.Position = WritePosition;
                try
                {
                    WriteStream.Write(BytesToWrite, 0, BytesToWrite.Length);
                }
                catch
                {
                    throw new IOException("Writing error " + Marshal.GetLastWin32Error());
                }

                try
                {
                    WriteStream.Dispose();
                    DeviceHandle.Dispose();
                }
                catch
                {
                    throw new IOException("Unable to close drive. Win32 Error Code " + Marshal.GetLastWin32Error());

                }
            }

        }



        public static bool CheckTRIM(string DeviceNamePath)
        {

            SafeFileHandle DeviceHandle = CreateFile(DeviceNamePath, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);


            if (DeviceHandle.IsInvalid)
            {
                throw new IOException("Unable to access drive. Win32 Error Code " + Marshal.GetLastWin32Error());
            }
            else
            {


                Win32Apti apti = new Win32Apti(DeviceHandle, true);
                //apti.ExecuteCommand;
                AtaDevice adevice = new AtaDevice(apti, true);
                DeviceIdentifier deviceinfo = new DeviceIdentifier(); // !it's a struct for data of device in SCSI lib we used.
                deviceinfo = adevice.IdentifyDevice();
                DeviceHandle.Close();
                DeviceHandle.Dispose();

                return deviceinfo.TrimCommandSupported;

            }
            
        }


        // this feature is not tested and completed.
        public static bool EraseDevice(string DeviceNamePath) 
        {
            SafeFileHandle DeviceHandle = CreateFile(DeviceNamePath, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);

            if (DeviceHandle.IsInvalid)
            {
                throw new IOException("Unable to access drive. Win32 Error Code " + Marshal.GetLastWin32Error());
                
            }

            else
            {
                if (CheckTRIM(DeviceNamePath)) 
                {
                    /*
                    Win32Apti apti = new Win32Apti(DeviceHandle, true);
                    AtaTaskFile atf = new AtaTaskFile();                // use another contstructor.
                    apti.ExecuteCommand(ref atf, ref atf, IntPtr.Zero, 0, 10, AtaFlags.DriveReadyRequired | AtaFlags.Command48Bit);
                     */
                DeviceHandle.Dispose();
                return true;
                }


                return false;              
            }
        }


        // may be unused
        public static StringCollection GetPhysicalDrivesNames()
        {
            var allDevices = Win32FileStream.QueryDosDevices(null);
            StringCollection DeviceNames = new StringCollection();
            Trace.Assert(allDevices != null, "Device array was null!");
            for (int i = 0; i < allDevices.Length; i++)
            {
                var m = System.Text.RegularExpressions.Regex.Match(allDevices[i], @"^(?<DEVICE>PhysicalDrive\d+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline | System.Text.RegularExpressions.RegexOptions.ExplicitCapture);
                if (m.Success)
                {
                    //var 
                    DeviceNames.Add(m.Groups["DEVICE"].Value);
                    Trace.Assert(DeviceNames != null, "Device name captured was null!");

                    //var devicePath = string.Format(@"\\.\{0}", deviceName); // need for get access to device
                }
            }
            return DeviceNames;
        }



    }



    // this class contains info about win32 device, geted by WMI

    
    class Device_Info
    {
        
        public Device_Info(string Caption, string DeviceID, string Description, string Manufacturer, string MediaType, string Model, string Name, string Size, string InterfaceType)
       {
           this.Caption = Caption;
           this.DeviceID = DeviceID;
           this.Description = Description;
           this.Manufacturer = Manufacturer;
           this.MediaType = MediaType;
           this.Model = Model;
           this.Name = Name;
           this.Size = Size;
           this.InterfaceType = InterfaceType;
       }
        public string Caption { private set; get; }
        public string DeviceID { private set; get; }
        public string Description { private set; get; }
        public string Manufacturer { private set; get; }
        public string MediaType { private set; get; }
        public string Model { private set; get; }
        public string Name { private set; get; }
        public string Size { private set; get; }
        public string InterfaceType { private set; get; }

       public enum Device_Type
        {
            Removable, 
            Fixed            
        };
         static System.Management.WqlObjectQuery query = new WqlObjectQuery("SELECT * FROM Win32_DiskDrive");
         static ManagementObjectSearcher result = new ManagementObjectSearcher(query);
         public static System.Collections.ArrayList devices = new System.Collections.ArrayList();

        //
        public static Device_Info[] GetPhysicalDevicesInfo(Device_Type type)
        {
            
                devices.Clear();

                foreach (var o in result.Get())
                {
                    // get only 'Fixed'/'Removable' devices
                    if (o["MediaType"].ToString().Contains(type.ToString()))
                    {
                        devices.Add(new Device_Info(o["Caption"].ToString(), o["DeviceID"].ToString(), o["Description"].ToString(), o["Manufacturer"].ToString(), o["MediaType"].ToString(), o["Model"].ToString(), o["Name"].ToString(), o["Size"].ToString(), o["InterfaceType"].ToString()));
                    }
                }
            
                return (Device_Info[])devices.ToArray(typeof(Device_Info));
                 
            
        }


    }


}
