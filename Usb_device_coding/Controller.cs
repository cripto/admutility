﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Windows.Forms;
using System.Threading;
namespace Usb_device_coding
{
    class Controller
    {
        static Thread generation_thread;
        public Controller()
        {
            
        }
               
        public Form1 FormViewShower()
        {
            Form1 form = new Form1();
            form.Show();
            return form;
        }
        
        // Get USB drives info.
        public static DriveInfo[] GetUSBDrives()
        {
            ArrayList USBDrives = new ArrayList();
            foreach (var Drives in DriveInfo.GetDrives())
            {
                if (Drives.IsReady && Drives.DriveType == DriveType.Removable)
                    USBDrives.Add(Drives);
            }
            
            return (DriveInfo[])USBDrives.ToArray(typeof(DriveInfo));
        }

        // Видалення таблиць за radiobutton
        public static void DeleteSqlLiteTable(string tablename)
        {
            DbSQLite2 del = new DbSQLite2();
            del.Pud_DeleteBD(tablename);
        }

        //Очистка таблиць radiobutton
        public static void ClearSqlLiteTable(string tablename)
        {
            DbSQLite2 clear = new DbSQLite2();
            clear.Pub_clearTable(tablename);
        }

        //створюємо бд
        public static void CreateSqlLiteDB()
        {
            DbSQLite2 date = new DbSQLite2();
            date.Pub_SetConnection();
        }

        //додавання даних в бд
        public static void AddDataToSqlLiteDB(string data1,string data2,string data3)
        {
            DbSQLite2 Add = new DbSQLite2();
            Add.Pub_SetConnection();// Перевіряєм чи створені таблиці- ні?Створюємо.
            Add.Pub_Add(data1, data2, data3);// Додаванян даних
        }

        //Старт генерації ключа
        // + bug with usb device file handle. "PhysicalDrive#" id must use for key record.
        public static void StartKeyGeneration(string algorithm,int keysizeinbytes, string deviceadress)
        {
           Generation gen = new Generation(algorithm, keysizeinbytes, deviceadress);
           generation_thread = new Thread(new ThreadStart(gen.Start));
           generation_thread.Start();
           
        }

        // Зупинка генерації ключа 
        public static void StopKeyGeneration()
        {
            try
            {
                generation_thread.Abort();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error of generation abort: " + ex.Message, "Error");
            }
        }


        // now work's in Windows only...
        public static ArrayList GetFixedDevices()
        {
            ArrayList devices = new ArrayList();
            Combobox_DataSource cmb;
            foreach (Device_Info deviceinfo in Device_Info.GetPhysicalDevicesInfo(Device_Info.Device_Type.Fixed))
            {
                cmb = new Combobox_DataSource(deviceinfo.Model, deviceinfo.DeviceID);
                devices.Add(cmb);
            }
            //return Device_Info.GetPhysicalDevicesInfo(Device_Info.Device_Type.Removable);
            return devices;
        }
        // now work's in Windows only...
        public static ArrayList GetRemovableDevices()
        {
            
                ArrayList devices = new ArrayList();
                Combobox_DataSource cmb;
                foreach (Device_Info deviceinfo in Device_Info.GetPhysicalDevicesInfo(Device_Info.Device_Type.Removable))
                {
                    cmb = new Combobox_DataSource(deviceinfo.Model, deviceinfo.DeviceID);
                    devices.Add(cmb);
                }
                //return Device_Info.GetPhysicalDevicesInfo(Device_Info.Device_Type.Removable);
                return devices;                         
        }

        //  test... now it's just chek TRIM on device

        public static void EraseSATADevice(string deviceID)
        {
            try
            {
                // if (Device_Writer.CheckTRIM(string.Format(@"\\.\{0}", erase_device_cb.SelectedItem.ToString())))
                if (Device_Writer.CheckTRIM(deviceID))
                {
                    MessageBox.Show("Trim is supported, erase will begin");
                    // Device_Writer.EraseDevice(string.Format(@"\\.\{0}", erase_device_cb.SelectedItem.ToString()));
                }
                else { MessageBox.Show("Trim function is not avialable for your device"); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error of selecting device"); }
        }




    }
}
