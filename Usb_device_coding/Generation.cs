﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CNG;
using System.Timers;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;



namespace Usb_device_coding
{
    class Genmethod : RandomNextGen
    {
        public Genmethod(string alghorytm): base (alghorytm) { }

        public override void NextBytes(byte[] buffer)
        {
            base.NextBytes(buffer);
        }
    
    }

    class Generation
    {
        byte[] generation_buffer;//= new byte[1024]; // filesize value(bytes)
        string argorithm;
        string generation_adress;
        private static System.Timers.Timer generationtimer;
       // public static int gentime;
        public static int Gentime { get; private set; }



       public Generation(string algorithm, int size, string device_adress)
        {
            generation_buffer = new byte[size];
            this.argorithm = algorithm;
            this.generation_adress = device_adress;
           
        }

        
        

        public void Start()
        {
            
            generationtimer = new System.Timers.Timer();
            generationtimer.Elapsed += new ElapsedEventHandler(Generation.generationtimer_Elapsed); generationtimer.Interval = 1000;// gentimer
            generationtimer.Start();
            //System.IO.File.WriteAllBytes(@generationadress + "Genfile.txt", Gen());
            
          

          /*  b = null;
           GC.Collect();
           */ // memory clean method 

            // Record(Gen(),generationadress);
           
           // writeToDisk(@"\\.\"+generationadress.Substring(0,2), Gen());

            //writeToDisk(@"\\.\I:", Generate());


            generation_buffer = Generate();
            File.WriteAllBytes(@"Test.txt",generation_buffer);
            //Device_Writer.Write(@"\\.\" + generation_adress.Substring(0, 2), 512, generation_buffer);
            Device_Writer.Write(generation_adress, 512, generation_buffer);
            generationtimer.Stop();
            MessageBox.Show("Generation completed for "+Gentime.ToString()+" seconds","Success");
            Gentime = 0;

        }

        // generate key 
       public byte[] Generate()
        {
            Genmethod nextbytes = new Genmethod(argorithm);  // CNG ALGHORYTM IDENTIFIER(?!)
         nextbytes.NextBytes(generation_buffer); 
         return (generation_buffer);
        }
        
        //generation time 
        static void generationtimer_Elapsed(object source, ElapsedEventArgs e)
        { 
            Gentime++;             
        }

        //byte record file
      /*  public void Record(byte[] array,string adress)
        {
            string filename=adress+"Genfiletest.txt";

            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                for (int i = 0; i < array.Length; i++)
                { 
                    fs.WriteByte(array[i]); 
                }
            }
            MessageBox.Show("Keyfile was created", "Success");
        }*/

        

    }
}
