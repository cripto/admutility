﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Collections;
using System.Management;



namespace Usb_device_coding
{

    public partial class Form1 : Form
    {
        bool clic;
        string flash;
        int iConBox=0;
        public static string deviceadress{get;private set;}
        Button key_but1;
        Device_Features Device_Features;
        DataGridView gridViewDevice = new DataGridView();
        Works_Data_Base Works_Data = new Works_Data_Base();

        
       // public static EventWaitHandle ewh = new EventWaitHandle(true, EventResetMode.ManualReset); //thread sync object!




        public Form1()
        {
            InitializeComponent();

            //key_but1 = new Button();
            Device_Features = new Device_Features(gridViewDevice);
            ///Підключені USB 
            ///
            //Пошук пыдключених USB
            /*
            foreach (var dInfo in Controller.GetUSBDrives())
            {

                int totsizBIT = Convert.ToInt32(dInfo.TotalSize / 1024);
                int totsizeMB = totsizBIT / 1024;
                int totsizeGB = totsizeMB / 1024;

                string AddConFlash = dInfo.Name + " " + dInfo.VolumeLabel + " | " + totsizeMB + " Mb";
                comboBox1.Items.Add(dInfo.Name + " " + dInfo.VolumeLabel + " | " + totsizeMB + " Mb");

               

            }
             */
            try
            {
                comboBox1.DataSource = Controller.GetRemovableDevices();
                comboBox1.SelectedItem = 0;
                comboBox1.DisplayMember = "DisplayMember";
                comboBox1.ValueMember = "ValueMember";
                deviceadress = comboBox1.SelectedValue.ToString();
            }
            catch
            {
                deviceadress = null;
            }
                 
            

            ///Підключені USB 



        }
        // Створення кнопки з картинок
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (clic == true)
            {
                pictureBox1.Image = Usb_device_coding.Properties.Resources.Без_имени_1;
                clic = false;
                return;
            }
            if (clic == false)
            {
                pictureBox1.Image = Usb_device_coding.Properties.Resources.Без_име21;
                clic = true;
                return;
            }
        }

        // Створює панелі з відповідним вмістом для потрібної задачі.
        private void button2_Click_1(object sender, EventArgs e)
        {
            panel_wind.BackColor = Color.LightPink;
            panel_wind.BringToFront();


           /* panel_wind.Controls.Remove(Works_Data.clear_table_button);
            panel_wind.Controls.Remove(Works_Data.alltables_rb);
            panel_wind.Controls.Remove(Works_Data.del_from_table_button);
            panel_wind.Controls.Remove(Works_Data.properties_table_label);
            panel_wind.Controls.Remove(Works_Data.properties_table_rb);
            panel_wind.Controls.Remove(Works_Data.logs_table_label);
            panel_wind.Controls.Remove(Works_Data.logs_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_table_label);
            panel_wind.Controls.Remove(Works_Data.key_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_size_label);
            panel_wind.Controls.Remove(Works_Data.key_size_textbox);
            panel_wind.Controls.Remove(Works_Data.table_operations_button);
            panel_wind.Controls.Remove(Works_Data.add_to_bd_button);
            panel_wind.Controls.Remove(Works_Data.key_label);
            panel_wind.Controls.Remove(Works_Data.Key_name_label);
            panel_wind.Controls.Remove(Works_Data.key_generation_button);
            panel_wind.Controls.Remove(Works_Data.key_textbox);
            panel_wind.Controls.Remove(Works_Data.key_name_textbox);*/
          

            // таблиця інфо
            panel_wind.Controls.Add(gridViewDevice);
            Device_Features.Fill(gridViewDevice);
            gridViewDevice.Visible = false;

            //---------------------------------------------// 
            panel_wind.Controls.Add(Works_Data.clear_table_button);
            panel_wind.Controls.Add(Works_Data.alltables_rb);
            panel_wind.Controls.Add(Works_Data.del_from_table_button);
            panel_wind.Controls.Add(Works_Data.properties_table_label);
            panel_wind.Controls.Add(Works_Data.properties_table_rb);
            panel_wind.Controls.Add(Works_Data.logs_table_label);
            panel_wind.Controls.Add(Works_Data.logs_table_rb);
            panel_wind.Controls.Add(Works_Data.key_table_label);
            panel_wind.Controls.Add(Works_Data.key_table_rb);

            //-- comment generation tools --//

           // panel_wind.Controls.Add(Works_Data.key_size_label);
           // panel_wind.Controls.Add(Works_Data.key_size_textbox);
            panel_wind.Controls.Add(Works_Data.table_operations_button);
            //panel_wind.Controls.Add(Works_Data.add_to_bd_button);
           // panel_wind.Controls.Add(Works_Data.key_label);
            //panel_wind.Controls.Add(Works_Data.Key_name_label);
            //panel_wind.Controls.Add(Works_Data.key_generation_button);
           // panel_wind.Controls.Add(Works_Data.key_textbox);
           // panel_wind.Controls.Add(Works_Data.key_name_textbox);
            //panel_wind.Controls.Add(Works_Data.button6);


            panel_wind.Controls.Add(Works_Data.generation_button);
            panel_wind.Controls.Add(Works_Data.start_generation_button);
            panel_wind.Controls.Add(Works_Data.stop_generation_button);
            panel_wind.Controls.Add(Works_Data.generation_algorithm_cb);
            panel_wind.Controls.Add(Works_Data.generation_size_cb);
            // add erasing tools

            panel_wind.Controls.Add(Works_Data.erase_button);
            panel_wind.Controls.Add(Works_Data.erase_device_cb);
            panel_wind.Controls.Add(Works_Data.start_erase_button);
            Works_Data.Add_elements();
        }

        // Удаляє попередній вміст панелі,Створює новий вміст для потрібної задачі.
        private void button3_Click(object sender, EventArgs e)
        {
          /* panel_wind.Controls.Remove(gridViewDevice);

            panel_wind.Controls.Remove(Works_Data.clear_table_button);
            panel_wind.Controls.Remove(Works_Data.alltables_rb);
            panel_wind.Controls.Remove(Works_Data.del_from_table_button);
            panel_wind.Controls.Remove(Works_Data.properties_table_label);
            panel_wind.Controls.Remove(Works_Data.properties_table_rb);
            panel_wind.Controls.Remove(Works_Data.logs_table_label);
            panel_wind.Controls.Remove(Works_Data.logs_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_table_label);
            panel_wind.Controls.Remove(Works_Data.key_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_size_label);
            panel_wind.Controls.Remove(Works_Data.key_size_textbox);
            panel_wind.Controls.Remove(Works_Data.table_operations_button);
            panel_wind.Controls.Remove(Works_Data.add_to_bd_button);
            panel_wind.Controls.Remove(Works_Data.key_label);
            panel_wind.Controls.Remove(Works_Data.Key_name_label);
            panel_wind.Controls.Remove(Works_Data.key_generation_button);
            panel_wind.Controls.Remove(Works_Data.key_textbox);
            panel_wind.Controls.Remove(Works_Data.key_name_textbox);
            panel_wind.Controls.Remove(key_but1);
            panel_wind.Controls.Remove(gridViewDevice);
            panel_wind.BackColor = Color.LemonChiffon;
            panel_wind.BringToFront();*/
        }

        // Удаляє попередній вміст панелі,Створює новий вміст для потрібної задачі.
        private void button4_Click(object sender, EventArgs e)
        {
            /*panel_wind.Controls.Remove(gridViewDevice);

            panel_wind.Controls.Remove(Works_Data.clear_table_button);
            panel_wind.Controls.Remove(Works_Data.alltables_rb);
            panel_wind.Controls.Remove(Works_Data.del_from_table_button);
            panel_wind.Controls.Remove(Works_Data.properties_table_label);
            panel_wind.Controls.Remove(Works_Data.properties_table_rb);
            panel_wind.Controls.Remove(Works_Data.logs_table_label);
            panel_wind.Controls.Remove(Works_Data.logs_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_table_label);
            panel_wind.Controls.Remove(Works_Data.key_table_rb);
            panel_wind.Controls.Remove(Works_Data.key_size_label);
            panel_wind.Controls.Remove(Works_Data.key_size_textbox);
            panel_wind.Controls.Remove(Works_Data.table_operations_button);
            panel_wind.Controls.Remove(Works_Data.add_to_bd_button);
            panel_wind.Controls.Remove(Works_Data.key_label);
            panel_wind.Controls.Remove(Works_Data.Key_name_label);
            panel_wind.Controls.Remove(Works_Data.key_generation_button);
            panel_wind.Controls.Remove(Works_Data.key_textbox);
            panel_wind.Controls.Remove(Works_Data.key_name_textbox);

            //panel_wind.Controls.Remove(lisBox1);
            panel_wind.Controls.Remove(key_but1);
            panel_wind.Controls.Remove(gridViewDevice);

            panel_wind.BackColor = Color.PaleGreen;
            panel_wind.BringToFront();*/
        }

        // ?!!!   Преревірка гаджету, та вивід стану (працездатний\ непрацездатний) каритнкою. 
        private void button1_Click(object sender, EventArgs e)
        {
            toolStripProgressBar1.Value = 0;
            while (toolStripProgressBar1.Value < 100)
            {
                toolStripProgressBar1.Value = toolStripProgressBar1.Value + 5;

                if (toolStripProgressBar1.Value == 100)
                {
                    pictureBox2.Visible = true;
                    Random ram = new Random();
                    int randomNumber = ram.Next(2);

                    if (randomNumber == 1)
                        pictureBox2.Image = Usb_device_coding.Properties.Resources.ok;
                    else
                        pictureBox2.Image = Usb_device_coding.Properties.Resources.err;
                }
            }

        }
        // При виборі гаджету стає активною кнопка "Перевірка пристрою"
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text != "")
            {
                button1.Enabled = true;
                deviceadress = comboBox1.SelectedValue.ToString();
            }
            else
            {
                button1.Enabled = false;
                deviceadress = null;            }
           // deviceadress=deviceadress.Substring(0, 3);
                        
        }

        // Вивід підказки
        private void Form1_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox1, "Click me...");
            button2_Click_1(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {

           /* panel_wind.Controls.Remove(gridViewDevice);

            panel_wind.Controls.Add(Works_Data.clear_table_button);
            panel_wind.Controls.Add(Works_Data.alltables_rb);
            panel_wind.Controls.Add(Works_Data.del_from_table_button);
            panel_wind.Controls.Add(Works_Data.properties_table_label);
            panel_wind.Controls.Add(Works_Data.properties_table_rb);
            panel_wind.Controls.Add(Works_Data.logs_table_label);
            panel_wind.Controls.Add(Works_Data.logs_table_rb);
            panel_wind.Controls.Add(Works_Data.key_table_label);
            panel_wind.Controls.Add(Works_Data.key_table_rb);
            panel_wind.Controls.Add(Works_Data.key_size_label);
            panel_wind.Controls.Add(Works_Data.key_size_textbox);
            panel_wind.Controls.Add(Works_Data.table_operations_button);
            panel_wind.Controls.Add(Works_Data.add_to_bd_button);
            panel_wind.Controls.Add(Works_Data.key_label);
            panel_wind.Controls.Add(Works_Data.Key_name_label);
            panel_wind.Controls.Add(Works_Data.key_generation_button);
            panel_wind.Controls.Add(Works_Data.key_textbox);
            panel_wind.Controls.Add(Works_Data.key_name_textbox);
            //panel_wind.Controls.Add(Works_Data.button6);
            Works_Data.Add_elements();
            panel_wind.BackColor = Color.SkyBlue;
            panel_wind.BringToFront();*/
            
        }
        
        //Вивід підключених пристроїв
        private const int WM_DEVICECHANGE = 0x0219;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const uint DBT_DEVTYP_VOLUME = 0x00000002;

        // [StructLayout(LayoutKind.Sequential)]
        private struct DEV_BROADCAST_HDR
        {
            public uint dbch_size;
            public uint dbch_devicetype;
            public uint dbch_reserved;
        }

        //[StructLayout(LayoutKind.Sequential)]
        private struct DEV_BROADCAST_VOLUME
        {
            public uint dbcv_size;
            public uint dbcv_devicetype;
            public uint dbcv_reserved;
            public uint dbcv_unitmask;
            public ushort dbcv_flags;
        }
         



        protected override void WndProc(ref Message m)
        {
            
            base.WndProc(ref m);
        
                if (m.Msg == WM_DEVICECHANGE)
                {
                    switch (m.WParam.ToInt32())
                    {
                        case DBT_DEVICEARRIVAL:
                            DEV_BROADCAST_HDR dbhARRIVAL = (DEV_BROADCAST_HDR)Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_HDR));
                            if (dbhARRIVAL.dbch_devicetype == DBT_DEVTYP_VOLUME)
                            {
                                
                                DEV_BROADCAST_VOLUME dbv = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_VOLUME));
                                BitArray bArray = new BitArray(new byte[] 
                            {
                                (byte)(dbv.dbcv_unitmask & 0x00FF),
                                (byte)((dbv.dbcv_unitmask & 0xFF00) >> 8),
                                (byte)((dbv.dbcv_unitmask & 0xFF0000) >> 16),
                                (byte)((dbv.dbcv_unitmask & 0xFF000000) >> 24)
                            });

                                int DriveLetter = Char.ConvertToUtf32("A", 0);
                                for (int i = 0; i < bArray.Length; i++)
                                {
                                    if (bArray.Get(i))
                                    {

                                        string ArrivedDevice = "";
                                        ArrivedDevice = Char.ConvertFromUtf32(DriveLetter) + @":\";
                                        DriveInfo drive = new DriveInfo(ArrivedDevice);//////
                                        int totsizBIT = Convert.ToInt32(drive.TotalSize/1024);
                                        //int totsizeBYTE = totsizBIT / 1024;
                                        int totsizeMB = totsizBIT / 1024;

                                       // comboBox1.Items.Add(ArrivedDevice + " " + drive.VolumeLabel + " | " + totsizeMB + " Mb");
                                        //flash = ArrivedDevice + " " + drive.VolumeLabel + " | " + totsizeMB + " Mb";
                                        //comboBox1.SelectedItem = flash;
                                        
                                        comboBox1.DataSource = null;
                                        Thread t = new Thread(delegate() { comboBox1.DataSource = Controller.GetRemovableDevices(); });
                                        t.Start(); t.Join();
                                        comboBox1.DisplayMember = "DisplayMember";
                                        comboBox1.ValueMember = "ValueMember";
                                        
                                        //MessageBox.Show(string.Format("{0} подключён.", ArrivedDevice));
                                        

                                    }

                                    DriveLetter += 1;
                                }
                            }
                           
                            break;
                        case DBT_DEVICEREMOVECOMPLETE:
                            DEV_BROADCAST_HDR dbhREMOVECOMPLETE = (DEV_BROADCAST_HDR)Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_HDR));
                            if (dbhREMOVECOMPLETE.dbch_devicetype == DBT_DEVTYP_VOLUME)
                            {
                                DEV_BROADCAST_VOLUME dbv = (DEV_BROADCAST_VOLUME)Marshal.PtrToStructure(m.LParam, typeof(DEV_BROADCAST_VOLUME));
                                BitArray bArray = new BitArray(new byte[] 
                            {
                                (byte)(dbv.dbcv_unitmask & 0x00FF),
                                (byte)((dbv.dbcv_unitmask & 0xFF00) >> 8),
                                (byte)((dbv.dbcv_unitmask & 0xFF0000) >> 16),
                                (byte)((dbv.dbcv_unitmask & 0xFF000000) >> 24)
                            });

                                int driveLetter = Char.ConvertToUtf32("A", 0);
                                for (int i = 0; i < bArray.Length; i++)
                                {
                                    if (bArray.Get(i))
                                    {

                                        //comboBox1.Items.Remove(flash);
                                        comboBox1.DataSource = null;
                                        Thread t = new Thread(delegate() { comboBox1.DataSource = Controller.GetRemovableDevices(); });
                                        t.Start(); t.Join();
                                        
                                        
                                        try
                                        {
                                            comboBox1.DisplayMember = "DisplayMember";
                                            comboBox1.ValueMember = "ValueMember";
                                        }
                                        catch
                                        {
                                            comboBox1.ResetText();
                                        }
                                        break;
                                        //MessageBox.Show("Отключен: " + Char.ConvertFromUtf32(driveLetter));
                                    }
                                    driveLetter += 1;
                                }
                            }
                            break;
                    }

                }
            }
         
         
        

        // device info
        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Works_Data.add_to_bd_button.Visible = false;
            Works_Data.alltables_rb.Visible = false;
            Works_Data.clear_table_button.Visible = false;
            Works_Data.create_bd_button.Visible = false;
            Works_Data.del_from_table_button.Visible = false;
            Works_Data.key_generation_button.Visible = false;
            Works_Data.key_label.Visible = false;
            Works_Data.Key_name_label.Visible = false;
            Works_Data.key_name_textbox.Visible = false;
            Works_Data.key_size_label.Visible = false;
            Works_Data.key_size_textbox.Visible = false;
            Works_Data.key_table_label.Visible = false;
            Works_Data.key_table_rb.Visible = false;
            Works_Data.key_textbox.Visible = false;
            Works_Data.logs_table_label.Visible = false;
            Works_Data.logs_table_rb.Visible = false;
            Works_Data.properties_table_label.Visible = false;
            Works_Data.properties_table_rb.Visible = false;
            Works_Data.table_operations_button.Visible = false;

            Works_Data.generation_button.Visible = false;
            Works_Data.start_generation_button.Visible = false;
            Works_Data.stop_generation_button.Visible = false;
            Works_Data.generation_algorithm_cb.Visible = false;
            Works_Data.generation_size_cb.Visible = false; 




            //panel_wind.Controls.Remove(lisBox1);
            //panel_wind.Controls.Remove(key_but1);

           /* panel_wind.Controls.Add(gridViewDevice);
            Device_Features.Fill(gridViewDevice);*/
            gridViewDevice.Visible = true;
        }

        private void panel_wind_MouseClick(object sender, MouseEventArgs e)
        {
            if (gridViewDevice.Visible==true)
            {               
                gridViewDevice.Visible = false;
                Works_Data.add_to_bd_button.Visible = true;
               // Works_Data.alltables_rb.Visible = false;
                //Works_Data.clear_table_button.Visible = false;
                Works_Data.create_bd_button.Visible = true;
                //Works_Data.del_from_table_button.Visible = false;
                Works_Data.key_generation_button.Visible = true;
                Works_Data.key_label.Visible = true;
                Works_Data.Key_name_label.Visible = true;
                Works_Data.key_name_textbox.Visible = true;
                Works_Data.key_size_label.Visible = true;
                Works_Data.key_size_textbox.Visible = true;
                //Works_Data.key_table_label.Visible = false;
               // Works_Data.key_table_rb.Visible = false;
                Works_Data.key_textbox.Visible = true;
                //Works_Data.logs_table_label.Visible = false;
               // Works_Data.logs_table_rb.Visible = false;
                //Works_Data.properties_table_label.Visible = false;
                //Works_Data.properties_table_rb.Visible = false;
                Works_Data.table_operations_button.Visible = true;

                Works_Data.generation_button.Visible = true;
            }

        }

        

       


        }
    }


