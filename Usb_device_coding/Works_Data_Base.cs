﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
namespace Usb_device_coding
{
    class Works_Data_Base
    {
        Thread generation_thread;
        public System.Windows.Forms.Panel panel_wind = new Panel();
        public Button clear_table_button = new Button();
        public RadioButton alltables_rb = new RadioButton();
        public Button del_from_table_button = new Button();
        public Label properties_table_label = new Label();
        public RadioButton properties_table_rb = new RadioButton();
        public Label logs_table_label = new Label();
        public RadioButton logs_table_rb = new RadioButton();
        public Label key_table_label = new Label();
        public RadioButton key_table_rb = new RadioButton();
        public Label key_size_label = new Label();
        public TextBox key_size_textbox = new TextBox();
        public Button table_operations_button = new Button();
        public Button add_to_bd_button = new Button();
        public Label key_label = new Label();
        public Button key_generation_button = new Button();
        public TextBox key_textbox = new TextBox();
        public TextBox key_name_textbox = new TextBox();
        public Button create_bd_button = new Button();
        public Label Key_name_label = new Label();
        //generation
        public Button generation_button = new Button();
        public Button start_generation_button = new Button();
        public Button stop_generation_button = new Button();
        public ComboBox generation_algorithm_cb = new ComboBox();
        public ComboBox generation_size_cb = new ComboBox();

        //erase device
        public Button erase_button = new Button();
        public Button start_erase_button = new Button();
        public ComboBox erase_device_cb = new ComboBox();

        public void Add_elements()
        {
         
            //Сворення тулсів
            //   
            // Key_name_label
            // 
            this.Key_name_label.AutoSize = true;
            this.Key_name_label.Location = new System.Drawing.Point(145, 81);
            this.Key_name_label.Name = "Key_name_label";
            this.Key_name_label.Size = new System.Drawing.Size(85, 15);
            this.Key_name_label.TabIndex = 3;
            this.Key_name_label.Text = "Назва_ключа";
            // 
            // clear_table_button
            // 
            clear_table_button.Location = new System.Drawing.Point(426, 187);
            clear_table_button.Name = "clear_table_button";
            clear_table_button.Size = new System.Drawing.Size(88, 23);
            clear_table_button.TabIndex = 18;
            clear_table_button.Text = "Очистити";
            clear_table_button.UseVisualStyleBackColor = true;
            clear_table_button.Visible = false;
            clear_table_button.Click += new System.EventHandler(clear_table_button_Click);
            // 
            // alltables_rb
            // 
            alltables_rb.AutoSize = true;
            alltables_rb.Location = new System.Drawing.Point(401, 148);
            alltables_rb.Name = "alltables_rb";
            alltables_rb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            alltables_rb.Size = new System.Drawing.Size(90, 19);
            alltables_rb.TabIndex = 17;
            alltables_rb.TabStop = true;
            alltables_rb.Text = "Усі таблиці";
            alltables_rb.UseVisualStyleBackColor = true;
            alltables_rb.Visible = false;
            // 
            // del_from_table_button
            // 
            del_from_table_button.Location = new System.Drawing.Point(285, 187);
            del_from_table_button.Name = "del_from_table_button";
            del_from_table_button.Size = new System.Drawing.Size(101, 23);
            del_from_table_button.TabIndex = 16;
            del_from_table_button.Text = "Удалити";
            del_from_table_button.UseVisualStyleBackColor = true;
            del_from_table_button.Visible = false;
            del_from_table_button.Click += new System.EventHandler(del_from_table_button_Click);
            // 
            // properties_table_label
            // 
            properties_table_label.AutoSize = true;
            properties_table_label.Location = new System.Drawing.Point(456, 69);
            properties_table_label.Name = "properties_table_label";
            properties_table_label.Size = new System.Drawing.Size(74, 30);
            properties_table_label.TabIndex = 15;
            properties_table_label.Text = "Таблиця \r\n\"Properties\"";
            properties_table_label.Visible = false;
            // 
            // properties_table_rb
            // 
            properties_table_rb.AutoSize = true;
            properties_table_rb.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            properties_table_rb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            properties_table_rb.Location = new System.Drawing.Point(480, 102);
            properties_table_rb.Name = "properties_table_rb";
            properties_table_rb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            properties_table_rb.Size = new System.Drawing.Size(34, 20);
            properties_table_rb.TabIndex = 14;
            properties_table_rb.Text = " ";
            properties_table_rb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            properties_table_rb.UseVisualStyleBackColor = true;
            properties_table_rb.Visible = false;
            // 
            // logs_table_label
            // 
            logs_table_label.AutoSize = true;
            logs_table_label.Location = new System.Drawing.Point(361, 78);
            logs_table_label.Name = "logs_table_label";
            logs_table_label.Size = new System.Drawing.Size(94, 15);
            logs_table_label.TabIndex = 13;
            logs_table_label.Text = "Таблиця \"Logs\"";
            logs_table_label.Visible = false;
            // 
            // logs_table_rb
            // 
            logs_table_rb.AutoSize = true;
            logs_table_rb.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            logs_table_rb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            logs_table_rb.Location = new System.Drawing.Point(388, 102);
            logs_table_rb.Name = "logs_table_rb";
            logs_table_rb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            logs_table_rb.Size = new System.Drawing.Size(34, 20);
            logs_table_rb.TabIndex = 12;
            logs_table_rb.Text = " ";
            logs_table_rb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            logs_table_rb.UseVisualStyleBackColor = true;
            logs_table_rb.Visible = false;
            // 
            // key_table_label
            // 
            key_table_label.AutoSize = true;
            key_table_label.Location = new System.Drawing.Point(268, 78);
            key_table_label.Name = "key_table_label";
            key_table_label.Size = new System.Drawing.Size(87, 15);
            key_table_label.TabIndex = 11;
            key_table_label.Text = "Таблиця \"Key\"";
            key_table_label.Visible = false;
            // 
            // key_table_rb
            // 
            key_table_rb.AutoSize = true;
            key_table_rb.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            key_table_rb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            key_table_rb.Location = new System.Drawing.Point(299, 102);
            key_table_rb.Name = "key_table_rb";
            key_table_rb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            key_table_rb.Size = new System.Drawing.Size(34, 20);
            key_table_rb.TabIndex = 10;
            key_table_rb.Text = " ";
            key_table_rb.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            key_table_rb.UseVisualStyleBackColor = true;
            key_table_rb.Visible = false;
            // 
            // key_size_label
            // 
            key_size_label.AutoSize = true;
            key_size_label.Location = new System.Drawing.Point(145, 152);
            key_size_label.Name = "key_size_label";
            key_size_label.Size = new System.Drawing.Size(47, 15);
            key_size_label.TabIndex = 9;
            key_size_label.Text = "Розмір";
            // 
            // key_size_textbox
            // 
            key_size_textbox.Location = new System.Drawing.Point(28, 149);
            key_size_textbox.Name = "key_size_textbox";
            key_size_textbox.ReadOnly = true;
            key_size_textbox.Size = new System.Drawing.Size(100, 20);
            key_size_textbox.TabIndex = 8;
            // 
            // table_operations_button
            // 
            table_operations_button.Location = new System.Drawing.Point(15, 4);
            table_operations_button.Name = "table_operations";
            table_operations_button.Size = new System.Drawing.Size(150, 46);
            table_operations_button.TabIndex = 7;
            table_operations_button.Text = "Операції з таблицями";
            table_operations_button.UseVisualStyleBackColor = true;
            key_table_label.Visible = false;
            logs_table_label.Visible = false;
            properties_table_label.Visible = false;
            key_table_rb.Visible = false;
            logs_table_rb.Visible = false;
            properties_table_rb.Visible = false;
            alltables_rb.Visible = false;
            del_from_table_button.Visible = false;
            clear_table_button.Visible = false;
            table_operations_button.Click += new System.EventHandler(table_operations_button_Click);
            // 
            // add_to_bd_button
            // 
            add_to_bd_button.Enabled = false;
            add_to_bd_button.Location = new System.Drawing.Point(28, 175);
            add_to_bd_button.Name = "add_to_bd_button";
            add_to_bd_button.Size = new System.Drawing.Size(75, 46);
            add_to_bd_button.TabIndex = 5;
            add_to_bd_button.Text = "Добавити в БД";
            add_to_bd_button.UseVisualStyleBackColor = true;
            add_to_bd_button.Click += new System.EventHandler(add_to_bd_button_Click);
            // 
            // key_label
            // 
            key_label.AutoSize = true;
            key_label.Location = new System.Drawing.Point(145, 120);
            key_label.Name = "key_label";
            key_label.Size = new System.Drawing.Size(37, 15);
            key_label.TabIndex = 4;
            key_label.Text = "Ключ";
            // 
            // key_generation_button
            // 
            key_generation_button.Location = new System.Drawing.Point(28, 14);
            key_generation_button.Name = "key_generation_button";
            key_generation_button.Size = new System.Drawing.Size(85, 51);
            key_generation_button.TabIndex = 2;
            key_generation_button.Text = "Генерация ключа";
            key_generation_button.UseVisualStyleBackColor = true;
            key_generation_button.Click += new System.EventHandler(key_generation_button_Click);
            // 
            // key_textbox
            // 
            key_textbox.Location = new System.Drawing.Point(28, 115);
            key_textbox.Name = "key_textbox";
            key_textbox.ReadOnly = true;
            key_textbox.Size = new System.Drawing.Size(100, 20);
            key_textbox.TabIndex = 1;
            // 
            // key_name_textbox
            // 
            key_name_textbox.Location = new System.Drawing.Point(28, 78);
            key_name_textbox.Name = "key_name_textbox";
            key_name_textbox.ReadOnly = true;
            key_name_textbox.Size = new System.Drawing.Size(100, 20);
            key_name_textbox.TabIndex = 0;


            // create_bd_button
            // 
            create_bd_button.Location = new System.Drawing.Point(154, 16);
            create_bd_button.Name = "create_bd_button";
            create_bd_button.Size = new System.Drawing.Size(75, 58);
            create_bd_button.TabIndex = 12;
            create_bd_button.Text = "Створити БД";
            create_bd_button.UseVisualStyleBackColor = true;
            create_bd_button.Click += new System.EventHandler(create_bd_button_Click);

            // generation_button
            //
            generation_button.Location = new System.Drawing.Point(15,55);
            generation_button.Name = "generation_button";
            generation_button.Size = new System.Drawing.Size(150,46);
            generation_button.TabIndex = 19;
            generation_button.Text = "Генерація";
            generation_button.UseVisualStyleBackColor = true;
            generation_button.Click += new System.EventHandler(generation_button_Click);
            
            //start_generation_button
            //
            start_generation_button.Location = new System.Drawing.Point(200, 100);
            start_generation_button.Name = "start_generation_button";
            start_generation_button.Size = new System.Drawing.Size(100,20);
            start_generation_button.TabIndex = 20;
            start_generation_button.Text = "Старт";
            start_generation_button.UseVisualStyleBackColor = true;
            start_generation_button.Visible = false;
            start_generation_button.Click += new System.EventHandler(start_generation_button_Click);

            //stop_generation_button
            //
            stop_generation_button.Location = new System.Drawing.Point(320, 100);
            stop_generation_button.Name = "stop_generation_button";
            stop_generation_button.Size = new System.Drawing.Size(100, 20);
            stop_generation_button.TabIndex = 20;
            stop_generation_button.Text = "Стоп";
            stop_generation_button.UseVisualStyleBackColor = true;
            stop_generation_button.Visible = false;
            stop_generation_button.Click += new System.EventHandler(stop_generation_button_Click);

            //generation_algorithm_cb.Name 
            //
            generation_algorithm_cb.Location = new System.Drawing.Point(200, 60);
            generation_algorithm_cb.Name = "generation_algorithm_cb.Name";
            generation_algorithm_cb.Size = new System.Drawing.Size(150, 20);
            generation_algorithm_cb.TabIndex = 21;
            generation_algorithm_cb.Text = "Алгоритм генерації";
            generation_algorithm_cb.Items.Add("RNG");
            generation_algorithm_cb.Items.Add("DUALECRNG");
            generation_algorithm_cb.Items.Add("FIPS186DSARNG");
            generation_algorithm_cb.SelectedIndex = 0;
            generation_algorithm_cb.Visible = false;

            //generation_size_cb
            //
            generation_size_cb.Location = new System.Drawing.Point(370,60);
            generation_size_cb.Name = "generation_size_cb";
            generation_size_cb.Size = new System.Drawing.Size(75, 20);
            generation_size_cb.TabIndex = 22;
            generation_size_cb.Text = "Розмір ключа";
            generation_size_cb.Visible = false;
            generation_size_cb.Items.Add("256 Mb");
            generation_size_cb.Items.Add("512 Mb");
            generation_size_cb.Items.Add("768 Mb");
            generation_size_cb.SelectedIndex = 0;



            // erase_button
            //
            erase_button.Location = new System.Drawing.Point(15, 106);
            erase_button.Name = "erase_button";
            erase_button.Size = new System.Drawing.Size(150, 46);
            erase_button.TabIndex = 23;
            erase_button.Text = "Стерти дані з пристрою";
            erase_button.UseVisualStyleBackColor = true;
            erase_button.Click += new System.EventHandler(erase_button_Click);
            
            // erase_device_cb
            //
            erase_device_cb.Location = new System.Drawing.Point(200, 106);
            erase_device_cb.Name = "erase_device_cb";
            erase_device_cb.Size = new System.Drawing.Size(250, 20);
            erase_device_cb.TabIndex = 21;
            erase_device_cb.Text = "Пристрій для стирання";
            // add items to erase_device_cb
            
            erase_device_cb.DataSource = Controller.GetFixedDevices();
            // set display and value members by property names of Device_Info class.
            erase_device_cb.DisplayMember = "DisplayMember";
            erase_device_cb.ValueMember = "ValueMember";
            
            erase_device_cb.SelectedIndex = 0;
            erase_device_cb.Visible = false;

            // start_erase_button
            //

            start_erase_button.Location = new System.Drawing.Point(200, 126);
            start_erase_button.Name = "start_generation_button";
            start_erase_button.Size = new System.Drawing.Size(100, 20);
            start_erase_button.TabIndex = 20;
            start_erase_button.Text = "Стерти дані";
            start_erase_button.UseVisualStyleBackColor = true;
            start_erase_button.Visible = false;
            start_erase_button.Click += new System.EventHandler(start_erase_button_Click);
        }

        //Вивід меню видалення таблиць
        public void table_operations_button_Click(object sender, EventArgs e)
        {
            key_table_label.Visible = !key_table_label.Visible;
            logs_table_label.Visible = !logs_table_label.Visible;
            properties_table_label.Visible = !properties_table_label.Visible;
            key_table_rb.Visible = !key_table_rb.Visible;
            logs_table_rb.Visible = !logs_table_rb.Visible;
            properties_table_rb.Visible = !properties_table_rb.Visible;
            alltables_rb.Visible = !alltables_rb.Visible;
            del_from_table_button.Visible = !del_from_table_button.Visible;
            clear_table_button.Visible = !clear_table_button.Visible;
        }


        //Видалення таблиць за RadioButton
        public void del_from_table_button_Click(object sender, EventArgs e)
        {
            string tabl1;
            if (key_table_rb.Checked == true)
            {
                tabl1 = "keys";
                /*
                DbSQLite2 del = new DbSQLite2();

                del.Pud_DeleteBD(tabl1);
                 */
                Controller.DeleteSqlLiteTable(tabl1);
            }

            if (logs_table_rb.Checked == true)
            {
                tabl1 = "logs";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pud_DeleteBD(tabl1);
                 */
                Controller.DeleteSqlLiteTable(tabl1);
            }

            if (properties_table_rb.Checked == true)
            {
                tabl1 = "propertties";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pud_DeleteBD(tabl1);
                 */
                Controller.DeleteSqlLiteTable(tabl1);
            }

            if (alltables_rb.Checked == true)
            {
                tabl1 = "keys";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pud_DeleteBD(tabl1);
                 */
                Controller.DeleteSqlLiteTable(tabl1);

                tabl1 = "logs";
                //del.Pud_DeleteBD(tabl1);
                Controller.DeleteSqlLiteTable(tabl1);
                tabl1 = "propertties";
                //del.Pud_DeleteBD(tabl1);
                Controller.DeleteSqlLiteTable(tabl1);
            }


        }

        //Очистка таблиць
        public void clear_table_button_Click(object sender, EventArgs e)
        {
            string tabl1;
            if (key_table_rb.Checked == true)
            {
                tabl1 = "keys";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pub_clearTable(tabl1);
                 */
                Controller.ClearSqlLiteTable(tabl1);
            }

            if (logs_table_rb.Checked == true)
            {
                tabl1 = "logs";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pub_clearTable(tabl1);
                 */
                Controller.ClearSqlLiteTable(tabl1);
            }

            if (properties_table_rb.Checked == true)
            {
                tabl1 = "propertties";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pub_clearTable(tabl1);
                 */
                Controller.ClearSqlLiteTable(tabl1);
            }

            if (alltables_rb.Checked == true)
            {
                tabl1 = "keys";
                /*
                DbSQLite2 del = new DbSQLite2();
                del.Pub_clearTable(tabl1);
                 */
                Controller.ClearSqlLiteTable(tabl1);
                tabl1 = "logs";
                //del.Pub_clearTable(tabl1);
                Controller.ClearSqlLiteTable(tabl1);

                tabl1 = "propertties";
                //del.Pub_clearTable(tabl1);
                Controller.ClearSqlLiteTable(tabl1);
            }
        }

        //Створює БД на клік
        public void create_bd_button_Click(object sender, EventArgs e)
        {
            try
            {
                /*
                DbSQLite2 date = new DbSQLite2();
                date.Pub_SetConnection();
                 */
                Controller.CreateSqlLiteDB();
                //button6.Visible = false;
                MessageBox.Show("База даних створилась / відновилась.");

            }
            catch
            {
                MessageBox.Show("База даних уже створена!");
                // button6.Visible = false;
            }
        }

        //Генерация ключа ???!!!
        public void key_generation_button_Click(object sender, EventArgs e)
        {
            Random key_rand = new Random();
            Random Siz = new Random();
            key_textbox.Text = "";
            key_name_textbox.Text = "";
            key_size_textbox.Text = "";
            int siz = Siz.Next(1000);
            key_size_textbox.Text = "" + siz;
            int key = key_rand.Next(100000000);
            key_textbox.Text = "" + key;

            for (int i = 0; i < 10; i++)
            {
                int name_key;
                name_key = key_rand.Next(97, 122);
                char cb = Convert.ToChar(name_key);
                key_name_textbox.Text = key_name_textbox.Text + cb;
            }
            add_to_bd_button.Enabled = true;
        }

        //Додавання даних в базу
        public void add_to_bd_button_Click(object sender, EventArgs e)
        {
            /*
            DbSQLite2 Add = new DbSQLite2();
            Add.Pub_SetConnection();// Перевіряєм чи створені таблиці- ні?Створюємо.
             */
            string add_date1 = key_name_textbox.Text;
            string add_date2 = key_textbox.Text;
            string add_date3 = key_size_textbox.Text;
            //Add.Pub_Add(add_date1, add_date2, add_date3);// Додаванян даних
            Controller.AddDataToSqlLiteDB(add_date1, add_date2, add_date3);
            key_name_textbox.Text = " ";
            key_textbox.Text = " ";
            key_size_textbox.Text = " ";
            add_to_bd_button.Enabled = false;
        }

        // Відкриття параметрів генерації
        private void generation_button_Click(object sender, EventArgs e)
        {
            generation_algorithm_cb.Visible = !generation_algorithm_cb.Visible;
            generation_size_cb.Visible = !generation_size_cb.Visible;
            start_generation_button.Visible = !start_generation_button.Visible;
            stop_generation_button.Visible = !stop_generation_button.Visible;
        }
        //старт генерації
        private void start_generation_button_Click(object sender, EventArgs e)
        {
            if (Form1.deviceadress!=null)
            {
                string algor = generation_algorithm_cb.Items[generation_algorithm_cb.SelectedIndex].ToString();
                string flashadress = Form1.deviceadress;
                if (generation_size_cb.SelectedIndex == 0)
                {
                    /*
                    Generation gen = new Generation(algor, 268435456, flashadress);
                    generation_thread = new Thread(new ThreadStart(gen.Start));
                    generation_thread.Start();
                     */
                    Controller.StartKeyGeneration(algor, 268435456, flashadress);
                }

                if (generation_size_cb.SelectedIndex == 1)
                {
                    /*
                    Generation gen = new Generation(algor, 536870912, flashadress);
                    generation_thread = new Thread(new ThreadStart(gen.Start));
                    generation_thread.Start();
                     */
                    Controller.StartKeyGeneration(algor, 536870912, flashadress);
                }
                if (generation_size_cb.SelectedIndex == 2)
                {
                    /*
                    Generation gen = new Generation(algor, 805306368, flashadress);
                    generation_thread = new Thread(new ThreadStart(gen.Start));
                    generation_thread.Start();
                     */
                    Controller.StartKeyGeneration(algor, 805306368, flashadress);
                }
            }
            else MessageBox.Show("Please select the device", "Error");
            
        }

        //стоп генерації
        private void stop_generation_button_Click(object sender, EventArgs e)
        {
          //generation_thread.Abort();
            Controller.StopKeyGeneration();            
        }

        // кнопка "Стерти дані з пристрою"
        private void erase_button_Click(object sender, EventArgs e)
        {
            erase_device_cb.Visible = !erase_device_cb.Visible;
            start_erase_button.Visible = !start_erase_button.Visible;
        }

        // кнопка "Старт стирання даних"
        private void start_erase_button_Click(object sender, EventArgs e)
        {
            /*
            try
            {
               // if (Device_Writer.CheckTRIM(string.Format(@"\\.\{0}", erase_device_cb.SelectedItem.ToString())))
                if(Device_Writer.CheckTRIM(erase_device_cb.SelectedValue.ToString()))
                {
                    MessageBox.Show("Trim is supported, erase will begin");
                    // Device_Writer.EraseDevice(string.Format(@"\\.\{0}", erase_device_cb.SelectedItem.ToString()));
                }
                else { MessageBox.Show("Trim function is not avialable for your device"); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error of selecting device"); }
             */
            Controller.EraseSATADevice(erase_device_cb.SelectedValue.ToString());
        }
        
    }
        
    }
