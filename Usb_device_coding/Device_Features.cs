﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

using System.Windows.Forms;

namespace Usb_device_coding
{
    class Device_Features
    {
        public 

        Device_Features(DataGridView dataGridView1)
        {
            dataGridView1.Location = new Point(22, 17);
            dataGridView1.Size = new System.Drawing.Size(370, 200);
            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[0].Name = "Property";
            dataGridView1.Columns[0].Width = 120;
            dataGridView1.Columns[1].Name = "Value";
            dataGridView1.Columns[1].Width = 200;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
        }


        public void Fill(DataGridView dataGridView1)
        {            
            try
            {
                // Заполнение таблицы
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add("Name", "Kingston");
                dataGridView1.Rows.Add("Size", "32 Gb");
            }
            catch
            {
                // Сообщаем пользователю о конкретной ошибке, а так же заносим характер ошибки в SQLite базу...
            }
        }
    }
}
