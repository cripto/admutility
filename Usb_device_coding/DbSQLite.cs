﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Usb_device_coding
{
    /// generic Singleton<T> (потокобезопасный с использованием generic-класса и с отложенной инициализацией)

    /// <typeparam name="T">Singleton class</typeparam>
    public class Singleton<T> where T : class
    {
        /// Защищённый конструктор необходим для того, чтобы предотвратить создание экземпляра класса Singleton. 
        /// Он будет вызван из закрытого конструктора наследственного класса.
        protected Singleton() { }

        /// Фабрика используется для отложенной инициализации экземпляра класса
        private sealed class SingletonCreator<S> where S : class
        {
            //Используется Reflection для создания экземпляра класса без публичного конструктора
            private static readonly S instance = (S)typeof(S).GetConstructor(
                        BindingFlags.Instance | BindingFlags.NonPublic,
                        null,
                        new Type[0],
                        new ParameterModifier[0]).Invoke(null);

            public static S CreatorInstance
            {
                get { return instance; }
            }
        }

        public static T Instance
        {
            get { return SingletonCreator<T>.CreatorInstance; }
        }

    }

    public class DbSQLite2: Singleton<DbSQLite2>
    {
        //public class DbSQLite1
       // {
            //private DbSQLite2() {}
             ListBox lisBox1 = new ListBox();
            Button key_but1 = new Button();
            private SQLiteConnection sql_connection;
            private SQLiteCommand sql_comand;
            private DataSet DSet = new DataSet();
            private DataTable DTable = new DataTable();
            Works_Data_Base Works_Data = new Works_Data_Base();


            //Підключення до Бази
            private void SetConnection()
            {
                string baseName = "D:\\Basekey.db3";
                sql_connection = new SQLiteConnection("Data Source=" + baseName + ";Version=3;New=False;Compress=True;");
            }

            //Публічне оголошення
            public void Pub_SetConnection()
            {
                CreateDbase();
            }

            // Створення бази даних
            private void CreateDbase()
            {
                SetConnection();
                sql_connection.Open();
                sql_comand = new SQLiteCommand(sql_connection);
                // "IF NOT EXISTS".Створює таблиці якщо вони не створені
                sql_comand.CommandText = @"create table if not exists [keys] ( 
                    [Id_key] integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    [Size] integer (100) NOT NULL,
                    [Id_write] text(100) NOT NULL,
                    [Body] blob NOT NULL
                    );

                    create table if not exists [logs] (
                    [Id_logs] integer PRIMARY KEY AUTOINCREMENT NOT NULL,
                    [Timestand] text(100) NOT NULL,
                    [Name_module] text(100) NOT NULL,
                    [Message] text(100) NOT NULL
                    );

                    create table if not exists [propertties] (
                    [Id_prop] integer PRIMARY KEY AUTOINCREMENT NOT NULL
                    );";
                sql_comand.CommandType = CommandType.Text;
                sql_comand.ExecuteNonQuery();
                sql_connection.Close();
            }

            //Видалення таблиць
            private void DeleteBD(string tabl)
            {
                try
                {
                    SetConnection();
                    sql_connection.Open();
                    sql_comand = new SQLiteCommand(sql_connection);
                    sql_comand.CommandText = @"drop table if exists  [" + tabl + "]; VACUUM";
                    sql_comand.CommandType = CommandType.Text;
                    sql_comand.ExecuteNonQuery();
                    sql_connection.Close();
                    MessageBox.Show("Таблиця '" + tabl + "' видаленна");
                }
                catch
                {
                    CreateDbase();
                    DeleteBD(tabl);
                }
            }

            //Публічне оголошення
            public void Pud_DeleteBD(string tabl)
            {
                DeleteBD(tabl);
            }

            //Очистка таблиць
            private void ClearTable(string tabl)
            {
                try
                {
                    SetConnection();
                    sql_connection.Open();
                    sql_comand = new SQLiteCommand(sql_connection);
                    sql_comand.CommandText = @"DELETE FROM  [" + tabl + "] WHERE 1; VACUUM";
                    sql_comand.CommandType = CommandType.Text;
                    sql_comand.ExecuteNonQuery();
                    sql_connection.Close();
                    MessageBox.Show("Таблиця '" + tabl + "' Очищена");
                    sql_connection.Close();
                }
                catch
                {
                    CreateDbase();
                    ClearTable(tabl);
                }
            }

            //Публічне оголошення
            public void Pub_clearTable(string tabl)
            {
                ClearTable(tabl);
            }
            //Створює БД на клік
            private void button6_Click(object sender, EventArgs e)
            {
                try
                {
                    CreateDbase();
                    // button6.Visible = false;
                }
                catch
                {
                    MessageBox.Show("База даних уже створена!");
                }
            }

            //Додавання даних у БД
            private void button8_Click(string d1, string d2, string d3)
            {
                string tex1 = d1;
                string tex2 = d2;
                string tex3 = d3;
                SetConnection();
                sql_connection.Open();
                sql_comand = new SQLiteCommand(sql_connection);
                string inser = @"INSERT INTO keys (Body,Id_write,Size) VALUES (@Body,@Id_write,@Size);";

                sql_comand.CommandText = inser;
                sql_comand.CommandType = CommandType.Text;
                sql_comand.Parameters.AddWithValue("@Body", tex1);
                sql_comand.Parameters.AddWithValue("@Id_write", tex2);
                sql_comand.Parameters.AddWithValue("@Size", tex3);
                sql_comand.ExecuteNonQuery();
                sql_connection.Close();
            }

            //Публічне оголошення
            public void Pub_Add(string dat1, string dat2, string dat3)
            {
                button8_Click(dat1, dat2, dat3);
            }
        //}
    }
}
